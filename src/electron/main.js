const {app, BrowserWindow, ipcMain} = require('electron');

let mainWindow = null;

global.sharedData = {
	deckDef: []
};

function createMainWindow() {
	let wnd = new BrowserWindow({width: 800, height: 600});

	wnd.loadURL(`file://${__dirname}/index.html`);
	wnd.on('closed', () => {
		mainWindow = null;
	});
	wnd.webContents.openDevTools(); // DEBUG:

	return wnd;
}

app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		app.quit();
	}
});
app.on('ready', () => {
	mainWindow = createMainWindow();
});
app.on('activate', () => {
	if (!mainWindow) {
		mainWindow = createMainWindow();
	}
});
ipcMain.on('angular-0', (event, arg) => {
	console.log('electron-0 received: ', arg);
	global.sharedData.deckDef = [];
	global.sharedData.deckDef.push('Electron main process');

	event.sender.send('electron-0', 'message from electron-0 to angular-0');
});
